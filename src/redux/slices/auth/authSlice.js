/*eslint-disable*/
import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import axios from 'axios';
import {BASE_URL} from '../../../globals/constant';


export const loginAsync = createAsyncThunk(
  'auth/loginAsync',
   async(payload, thunkAPI) => {
     const axiosInstance = axios.create({
        baseURL: BASE_URL,
        timeout: 5000,
      });

       try{
        const res = await axiosInstance.post("api/v1/customers/login", {
          username: payload.username,
          password: payload.password,
        });
        if(res.data.success == true)
        {
          return {
            message: 'ok',
            current: res.data.data
        }
        }else{
          return {
            message: 'fail',
        }
        }
        
      }catch(err){
        if (JSON.stringify(err).includes('message')) {
            alert(err.response.data.message);
            console.log(err)
            return {
                message: err.response.message
            }
          } else {
            alert('LOGIN FAIL 2:\n' + err);
            return{
                message: err+''
            }
          }
      }
   }
);

export const logoutAsync = createAsyncThunk(
    'auth/logoutAsync',
     async(payload, thunkAPI) => {
       const axiosInstance = axios.create({
          baseURL: BASE_URL,
          timeout: 5000,
          headers: {
            Authorization: 'Bearer ' + payload.accessToken,
          },
        });
         //console.log("ok nè")
         try{
          const res = await axiosInstance.post("api/v1/customers/logout");
          if(res.data.success+"" == "true")
          return {
              message: 'ok',
          }
        }catch(err){
          if (JSON.stringify(err).includes('message')) {
              if(err.code = "ERR_NETWORK")
              {
                alert("Server is not running");
                // localStorage.removeItem("fptEcommerce_customerAddress_id")
                // localStorage.removeItem("fptEcommerce_customerAddress_location") 
                // localStorage.removeItem("persist:root")
                return;
              }
              alert(err.response.data.message);
              return {
                  message: err.response.message
              }
            } else {
              alert('LOGOUT FAIL:\n');
              return{
                  message: err+''
              }
            }
        }
     }
  );

export const updateInfoAsync = createAsyncThunk(
    'auth/updateInfoAsync',
     async(payload, thunkAPI) => {
       const axiosInstance = axios.create({
          baseURL: BASE_URL,
          timeout: 5000,
          headers: {
            Authorization: 'Bearer ' + payload.accessToken,
          },
        });
  
         try{
          const res = await axiosInstance.post("api/v1/customers/update-info", {
            fullName: payload.fullname,
            email: payload.email,
            phone: payload.phone
          });
          if(res.data.success+"" == "true")
          return {
              message: 'ok',
              fullname: payload.fullname,
              email: payload.email,
              phone: payload.phone
          }
        }catch(err){
          console.log(err)
          if (JSON.stringify(err).includes('message')) {
            if(err.code = "ERR_NETWORK")
            {
              alert("Server is not running");
              // localStorage.removeItem("fptEcommerce_customerAddress_id")
              // localStorage.removeItem("fptEcommerce_customerAddress_location") 
              // localStorage.removeItem("persist:root")
              return;
            }
            alert(err.response.data.message);
              return {
                  message: err.response.message
              }
            } else {
              alert('UPDATE FAIL:\n' + err);
              return{
                  message: err+''
              }
            }
        }
     }
  );

// export const changeInfoAsync = createAsyncThunk(
//     'auth/changeInfoAsync',
//      async(payload, thunkAPI) => {
//         try{
//             const arr = [...payload.whatToLearn].map(i => i.id);
//             const arr1 = [...payload.whatToLearn1].map(i => i.id);
          
//             const axiosInstance1 = axios.create({
//                 baseURL: BASE_URL,
//                 timeout: 5000,
//                 headers: {
//                   Authorization: 'Bearer ' + payload.accessToken,
//                 },
//               });
//             const res = await axiosInstance1.put("user/info", {
//                 birthday: payload.birthday,
//                 country: payload.country,
//                 language: payload.language,
//                 learnTopics: arr,
//                 level: payload.level,
//                 name: payload.name,
//                 phone: payload.phone,
//                 testPreparations: arr1,
//             });
//             return {
//                 message: 'ok',
//                 user: res.data.user
//             }
//           }catch(err){
//             alert(err)
//             return {
//                 message: err,
//             }
//           }
//      }
//   );

// export const changeAvatar = createAsyncThunk(
//     'auth/changeAvatar',
//     async(payload, thunkAPI) => {
//         try{
//          const res = await axiosInstance.post("auth/login", {
//            email: payload.email,
//            password: payload.password,
//          });
//          return {
//              message: 'ok',
//              current: res.data
//          }
//        }catch(err){
//         if (JSON.stringify(err).includes('message')) {
//             alert('FAIL:\n' + err.response.data.message);
//             return {
//                 message: err.response.data.message
//             }
//           } else {
//             alert('FAIL:\n' + err);
//             return {
//                 message: err + ''
//             }
//           }
//        }
//     }
// )

const authSlice = createSlice({
    name: 'auth',
    initialState: {
        current: {  //current: accessToken & user
        },
        isLoggin: false,
    },
    reducers:{
        init: (state, action) =>{
            state.isLoggin = true;
        },
        login: (state, action) => {
          if(action.payload.message == 'ok'){
            state.current = action.payload.current; // = currentUser
            state.isLoggin = true;
        }
        },
        updateInfo: (state, action) => {
          if(action.payload.message == 'ok'){
            var oldUser = state.current.user;
            state.current.user = {...oldUser, fullName: action.payload.fullname, email: action.payload.email, phone: action.payload.phone}; // = currentUser
          }
        },
        logout: (state, action) =>{
          if(action.payload.message == 'ok'){
            state.current = {}; // = currentUser
            state.isLoggin = false;
        }
        },
        initNew: (state, action) => {
            state.current = action.payload.current; // = currentUser
            state.isLoggin = true;
        },
    },
    extraReducers:{
        [loginAsync.fulfilled]: (state, action) => {
            if(action.payload.message == 'ok'){
                state.current = action.payload.current; // = currentUser
                state.isLoggin = true;
            }
        },
        [logoutAsync.fulfilled]: (state, action) => {
          if(action.payload.message == 'ok'){
              state.current = {}; // = currentUser
              state.isLoggin = false;
          }
      },
        [updateInfoAsync.fulfilled]: (state, action) => {
          // alert(JSON.stringify(action.payload))
            if(action.payload.message == 'ok'){
                var oldUser = state.current.user;
                state.current.user = {...oldUser, fullName: action.payload.fullname, email: action.payload.email, phone: action.payload.phone}; // = currentUser
                // console.log(state.current.user)
              }
        },
        // [changeInfoAsync.fulfilled]: (state, action) => {
        //     if(action.payload.message == 'ok'){
        //         state.current = {
        //             ...state.current,
        //             user: action.payload.user
        //         }
        //         state.isLoggin = true;
        //     }
        // }
    }
})

export const {init, login, logout, initNew, updateInfo} = authSlice.actions;
export default authSlice.reducer;
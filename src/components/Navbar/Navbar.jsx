/* eslint-disable */
import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link, useLocation, NavLink, useNavigate } from "react-router-dom";
import { logout } from "../../redux/slices/auth/authSlice";
import { useSelector, useDispatch } from "react-redux";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { BASE_URL } from "../../globals/constant";

export default function Navbar(props) {
  const location = useLocation();
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const pathName = location.pathname;

  const totalQuantity = useSelector((state) => state.cart.totalQuantity);
  const isLoggin = useSelector((state) => state.auth.isLoggin);
  const current = useSelector((state) => state.auth.current);

  const [search, setSearch] = useState("");
  const [search1, setSearch1] = useState("");

  const notify = (message) =>
    toast.error(message, {
      position: toast.POSITION.TOP_CENTER,
    });

  useEffect(() => {
    if (search1 != "") {
      navigate(`/products/pages?search=${search1}&perPage=5&currentPage=1`);
    }
  }, [search1]);

  const handleClick = (e) => {
    const axiosInstance = axios.create({
      baseURL: BASE_URL,
      timeout: 10000,
      headers: {
        Authorization: "Bearer " + current.accessToken,
      },
    });
    axiosInstance
      .post("api/v1/customers/logout")
      .then((res) => {
        if (res.data.success + "" == "true")
          dispatch(
            logout({
              message: "ok",
            })
          );
        localStorage.removeItem("fptEcommerce_customerAddress_id");
        localStorage.removeItem("fptEcommerce_customerAddress_location");
        localStorage.removeItem("persist:root");
        navigate("/");
      })
      .catch((err) => {
        if ((err.response.status = 401)) {
          localStorage.removeItem("fptEcommerce_customerAddress_id");
          localStorage.removeItem("fptEcommerce_customerAddress_location");
          localStorage.removeItem("persist:root");
          navigate("/login/home");
        } else {
          notify(err.response.data.message);
        }
      });
  };

  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-light bg-light py-3">
        <div className="container-fluid">
          {/* <div class="d-inline-block ms-5">
          <Link className="navbar-brand fw-bold fs-4" to="/">
            FPT-ECommerce
          </Link>
          </div> */}
          <Link className="navbar-brand fw-bold fs-4 ms-5" to="/">
            FPT-ECommerce
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mx-auto mb-2 mb-lg-0">
              <li className="nav-item">
                {pathName == "/" ? (
                  <NavLink to="/" className="nav-link active">
                    Home
                  </NavLink>
                ) : (
                  <NavLink to="/" className="nav-link">
                    Home
                  </NavLink>
                )}
              </li>
              <li className="nav-item">
                {pathName == "/products" ? (
                  <NavLink
                    to="/products/pages?currentPage=1"
                    className="nav-link active"
                  >
                    Product
                  </NavLink>
                ) : (
                  <NavLink
                    to="/products/pages?currentPage=1"
                    className="nav-link"
                  >
                    Product
                  </NavLink>
                )}
              </li>
              <li className="nav-item">
                <NavLink to="/about" className="nav-link">
                  About
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/contact" className="nav-link">
                  Contact
                </NavLink>
              </li>
              <li style={{ marginLeft: 150 }}>
                {/* <div className="input-group search">
                <div className="form-outline">
                  <input type="search" id="form1" className="form-control" value={search} onChange={(e)=>setSearch(e.target.value)}/>
                  
                </div>
                <button type="button" className="btn btn-primary" onClick={() => {
                  //alert(search)
                  setSearch1(search)
                  //navigate(`/products/pages?search=${search}&perPage=5&currentPage=1`);
                }}>
                  <i className="fas fa-search" />
                </button>
              </div> */}
                <div className="input-group">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Search..."
                    aria-describedby="basic-addon1"
                    value={search}
                    onChange={(e) => setSearch(e.target.value)}
                  />
                  <button
                    className="input-group-text btn btn-primary"
                    id="basic-addon1"
                    onClick={() => {
                      setSearch1(search);
                    }}
                  >
                    <i className="fa-solid fa-magnifying-glass"></i>
                  </button>
                </div>
              </li>
            </ul>
            <div>
              <div className="dropdown d-inline-block">
                <li
                  className="nav-link dropdown-toggle btn btn-outline-dark ms-2"
                  id="navbarDropdown"
                  data-bs-toggle="dropdown"
                >
                  <i className="fa-solid fa-user-group" />{" "}
                  {isLoggin == true ? current.user.username : "User"}{" "}
                </li>
                <ul
                  className="dropdown-menu dropdown-menu-end"
                  aria-labelledby="navbarDropdown"
                >
                  {isLoggin == false ? (
                    <Link className="dropdown-item" to="/login/home">
                      Login
                    </Link>
                  ) : (
                    <li className="dropdown-item" onClick={handleClick}>
                      Logout
                    </li>
                  )}
                </ul>
              </div>
              <Link to="/cart" className="btn btn-outline-dark ms-2 me-5">
                <div>
                  <i className="fa-solid fa-cart-shopping" /> Cart{" "}
                  <span style={{ color: "red" }}>({totalQuantity})</span>
                </div>
              </Link>
            </div>
          </div>
        </div>
      </nav>
      <ToastContainer />
    </div>
  );
}

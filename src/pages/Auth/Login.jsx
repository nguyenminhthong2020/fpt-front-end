import React from "react";

import { useParams, useNavigate } from "react-router-dom";
import axios from "axios";
import { login } from "../../redux/slices/auth/authSlice";
import { useDispatch } from "react-redux";
import { useForm} from "react-hook-form";
import { BASE_URL } from "../../globals/constant";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function Login(props) {
  const { returnUrl } = useParams();

  const {
    register,
    // control,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const notify = (message) =>
    toast.error(message, {
      position: toast.POSITION.TOP_CENTER,
    });

  const onSubmit = (data) => {
    const axiosInstance = axios.create({
      baseURL: BASE_URL,
      timeout: 10000,
    });
    axiosInstance
      .post("api/v1/customers/login", {
        username: data.username,
        password: data.password,
      })
      .then((res) => {
        if (res.data.success + "" == "true") {
          dispatch(
            login({
              message: "ok",
              current: res.data.data,
            })
          );
          if (returnUrl == "home") {
            navigate("/");
          }
        } else {
          notify(res.data.message);
        }
      })
      .catch((err) => {
        notify(err.response.data.message);
      });
  };

  return (
    <div className="container" style={{ marginTop: 50 }}>
      <div
        className="row"
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
        }}
      >
        <div className="col-sm-12 col-md-8 col-lg-6">
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="login-form">
              <h2
                className="login-title"
                style={{ fontFamily: "Times New Roman" }}
              >
                ĐĂNG NHẬP
              </h2>
              <div className="row ">
                <div className="col-md-12 pb-3">
                  <label>Nhập Username</label>
                  <input
                    type="text"
                    id="username"
                    name="username"
                    {...register("username", {
                      required: true,
                      pattern: {
                        value: /^[A-Za-z0-9]+$/i,
                        message: "Please enter a valid username",
                      },
                    })}
                  />
                  {errors.username?.type == "required" && (
                    <span
                      style={{
                        textAlign: "center",
                        color: "red",
                      }}
                    >
                      Username is required
                    </span>
                  )}
                  {errors.username?.type == "pattern" && (
                    <span
                      style={{
                        textAlign: "center",
                        color: "red",
                      }}
                    >
                      Username is not valid
                    </span>
                  )}
                </div>

                <div className="col-md-12 pb-3">
                  <label>Nhập Password</label>
                  <input
                    type="password"
                    id="password"
                    name="password"
                    {...register("password", {
                      required: true,
                    })}
                  />
                  {errors.password?.type === "required" && (
                    <span
                      style={{
                        textAlign: "center",
                        color: "red",
                      }}
                    >
                      Password is required
                    </span>
                  )}
                </div>

                <div className="col-12">
                  <button
                    type="submit"
                    className="btn btn-custom-size lg-size btn-secondary btn-primary-hover rounded-0"
                  >
                    Đăng nhập
                  </button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <ToastContainer />
    </div>
  );
}

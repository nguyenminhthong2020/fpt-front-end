/*eslint-disable*/
import React, { useState, useEffect } from "react";
import Navbar from "../../../components/Navbar/Navbar";

import {
  Link,
  useParams,
  useNavigate,
  useSearchParams,
} from "react-router-dom";
import ReactPaginate from "react-paginate";
import WebFooter from "../../../components/WebFooter/WebFooter";

const PER_PAGE = 5;

export default function Products(props) {
  // const { page, search } = useParams();
  const [searchParams, setSearchParams] = useSearchParams();
  let search = searchParams.get("search");
  let currentPage = searchParams.get("currentPage");


  if (search == null || search == undefined) {
    search = "";
  }


  const navigate = useNavigate();
  // console.log(page);

  const [data, setData] = useState({
    total: 0,
    items: [],
  });
  //const [currentPage, setCurrentPage] = useState(page);
  const [loading, setLoading] = useState(false);

  let componentMounted = true;

  useEffect(() => {
    const getProducts = async () => {
      setLoading(true);

      const res = await fetch(
        `https://localhost:44362/api/v1/products/pages?search=${search}&perPage=${PER_PAGE}&currentPage=${currentPage}`
      );
      const res1 = await res.json();

      //console.log(res1);
      if (componentMounted) {
        setData({
          total: res1.data.total,
          items: res1.data.items,
        });
        setLoading(false);
      }

      return () => {
        componentMounted = false;
      };
    };

    getProducts();
  }, [currentPage, search]);

  const Loading = () => {
    return <div>Loading...</div>;
  };

  // function handlePageClick({ selected: selectedPage }) {
  //   //setCurrentPage(selectedPage);

  // }
  const handlePageClick = ({ selected: selectedPage }) =>
    navigate(`/products/pages?search=${search}&currentPage=${selectedPage + 1}`);

  //const offset = currentPage * PER_PAGE;

  const pageCount = Math.ceil(data.total / PER_PAGE);

  return (
    <div>
      <Navbar />
      <div className="container mt-4">
        <div className="row">
          <div className="col-md-12">
            <div className="tab-content text-charcoal pt-8">
              <div
                className="tab-pane fade show active"
                id="list-view"
                role="tabpanel"
                aria-labelledby="list-view-tab"
              >
                <div className="product-list-view with-sidebar row">
                  {loading == false ? (
                    data.items.map(function (p) {
                      return (
                        <Link
                          to={`/products/${p.productID}`}
                          className="col-12 pb-6"
                          key={p.productID}
                        >
                          <div className="product-item">
                            <div className="product-img img-zoom-effect">
                              <div>
                                <img
                                  className="img-full"
                                  src={p.thumb}
                                  alt={p.description}
                                />
                              </div>
                            </div>
                            <div className="product-content align-self-center">
                              <div
                                className="product-name pb-2 listProductItem"
                                style={{
                                  textDecoration: "none",
                                  fontWeight: "bold",
                                }}
                              >
                                {p.name}
                              </div>
                              <div className="price-box pb-1">
                                <span className="new-price text-danger">
                                  {p.price} USD
                                </span>
                              </div>
                              <p className="short-desc mb-0">{p.description}</p>
                            </div>
                          </div>
                        </Link>
                      );
                    })
                  ) : (
                    <Loading></Loading>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br />
      <ReactPaginate
        previousLabel={"← Previous"}
        nextLabel={"Next →"}
        pageCount={pageCount}
        onPageChange={handlePageClick}
        containerClassName={"pagination"}
        previousLinkClassName={"pagination__link"}
        nextLinkClassName={"pagination__link"}
        disabledClassName={"pagination__link--disabled"}
        activeClassName={"pagination__link--active"}
      />
      <br />
      <br />
      <WebFooter />
    </div>
  );
}

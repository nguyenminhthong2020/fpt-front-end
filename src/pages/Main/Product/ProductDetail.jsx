/* eslint-disable */
import React, { useState, useEffect } from "react";

import { useDispatch } from "react-redux";
import { addCart } from "../../../redux/slices/cart/cartSlice";

import Navbar from "../../../components/Navbar/Navbar";

import { useParams, Link, NavLink } from "react-router-dom";
import WebFooter from "../../../components/WebFooter/WebFooter";

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default function ProductDetail(props) {
  const { id } = useParams();

  const dispatch = useDispatch();
  //const cart = useSelector(state => state.cart);

  const [product, setProduct] = useState({});

  const [quantity, setQuantity] = useState(0);

  const notify = () => toast.success("Add to cart successfully!",
  {
    position: toast.POSITION.TOP_CENTER
  });


  useEffect(() => {
    const getProduct = async () => {
      const res = await fetch(`https://localhost:44362/api/v1/products/${id}`);
      
      const res1 = await res.json();

      setProduct(res1.data);
    };

    getProduct();
  }, []);

  const addToCart = () => {
    if (quantity > 0) {
      let newProduct = {
        id: product.productID,
        product: product,
        quantity: quantity,
      };
      dispatch(addCart(newProduct));
      notify();
    }
  };

  const ShowProduct = (props) => {
    return (
      <>
        <div className="col-md-6">
          <img
            src={product.thumb}
            alt={product.description}
            height="400px"
            width="400px"
          />
        </div>
        <div className="col-md-6">
          <h1 className="display-5 text-primary">{product.name}</h1>
          <h3 className="display-6 fw-bold my-4">{product.price} USD</h3>
          <p className="lead">{product.description}</p>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <div
              style={{
                backgroundColor: "lightgrey",
                paddingLeft: 5,
                paddingRight: 5,
              }}
            >
              <i
                className="fa-solid fa-minus"
                onClick={() => {
                  if (quantity > 0) {
                    setQuantity(quantity - 1);
                  }
                }}
              ></i>
              <input
                defaultValue={quantity}
                type="text"
                style={{
                  textAlign: "center",
                  width: 45,
                  border: "none",
                  height: 42,
                  color: "red",
                  backgroundColor: "lightgrey",
                }}
              />
              <i
                className="fa-solid fa-plus"
                onClick={() => {
                  setQuantity(quantity + 1);
                }}
              ></i>
            </div>

            <button className="btn btn-dark ms-5 myBtn" onClick={addToCart}>
              Add to Cart
            </button>
            <NavLink className="btn btn-dark ms-3 myBtn" to="/cart">
              Go to Cart
            </NavLink>
          </div>
        </div>
      </>
    );
  };

  return (
    <div>
      <Navbar></Navbar>
      <div className="container mt-5 mb-5">
        <div className="row">
          <ShowProduct></ShowProduct>
          <ToastContainer />
        </div>
      </div>
      <WebFooter/>
    </div>
  );
}

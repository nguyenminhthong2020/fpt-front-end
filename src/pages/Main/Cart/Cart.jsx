import React from "react";
import Navbar from "../../../components/Navbar/Navbar";
import { removeProduct } from "../../../redux/slices/cart/cartSlice";

import { useSelector, useDispatch } from 'react-redux';

import {
  Link,
  
} from "react-router-dom";

export default function Cart(props) {
  const dispatch = useDispatch();
  const cart = useSelector(state => state.cart.cart);
  let sum = cart.reduce((partialSum, a) => partialSum + a.product.price * a.quantity, 0);

  const handleRemoveProduct = (id, quantity) => {
     dispatch(removeProduct({
         id: id,
         quantity: quantity
     }))
  }

  const ListProduct = ({handleRemoveProduct}) => {

      return cart.map(function(p){
          return (
            <tr key={p.product.productID}>
            <td className="product_remove">
                <i
                  onClick={() => handleRemoveProduct(p.product.productID, p.quantity)}
                  className="fa-regular fa-circle-xmark"
                  style={{ fontSize: 25 }}
                />
            </td>
            <td className="product-thumbnail">
              <Link to={`/products/${p.product.productID}`}>
                <img
                  src={p.product.thumb}
                  alt={p.product.name}
                  width={112}
                  height={124}
                />
              </Link>
            </td>
            <td>
              <Link
                to={`/products/${p.product.productID}`}
                style={{ fontSize: "1.2em", fontWeight: "bold" }}
              >
                {p.product.name}{" "}
              </Link>
            </td>
            <td className="product-price">
              <span className="amount text-danger">{p.product.price} VNĐ</span>
            </td>
            <td className="quantity">
              <div
                className="cart-plus-minus updateCart"
                id={41}
                data-mahh={41}
              >
                <input
                  className="cart-plus-minus-box"
                  defaultValue={p.quantity}
                  type="text"
                  id="amount_41"
                />
                <div className="dec qtybutton">
                  <i className="fa fa-minus" aria-hidden="true" />
                </div>
                <div className="inc qtybutton">
                  <i className="fa fa-plus" aria-hidden="true" />
                </div>
              </div>
            </td>
            <td className="product-subtotal">
              <span className="amount text-danger">{p.product.price * p.quantity} VNĐ</span>
            </td>
          </tr>
          )
      })
  }


  return (
    <div>
     <Navbar/>
      <div className="cart-area section-space-y-axis-100">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <form action="">
                <div className="table-content table-responsive">
                  <table className="table">
                    <thead>
                      <tr>
                        <th colSpan={3}>Sản phẩm</th>
                        <th>Giá</th>
                        <th>Số lượng</th>
                        <th>Tạm tính</th>
                      </tr>
                    </thead>
                    <tbody>
                     <ListProduct handleRemoveProduct={handleRemoveProduct}/>
                    </tbody>
                  </table>
                </div>
                <div className="row">
                  <div className="col-md-5 ml-auto">
                    <div className="cart-page-total">
                      <h2>CỘNG GIỎ HÀNG</h2>
                      <ul>
                        <li>
                          Tổng cộng <span>{sum} VNĐ</span>
                        </li>
                      </ul>
                      {
                        sum > 0 && <Link
                        to="/customer-information"
                        className="btn btn-secondary btn-primary-hover"
                      >
                        Thanh toán
                      </Link>
                      }
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

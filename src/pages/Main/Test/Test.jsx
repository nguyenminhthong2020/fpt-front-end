import React from 'react'
import { useParams, useSearchParams} from "react-router-dom";
import Navbar from '../../../components/Navbar/Navbar';

export default function Test(props) {
    const [searchParams, setSearchParams] = useSearchParams();
    const s = searchParams.get('search')
    const p = searchParams.get('page');

    return (
        <>
            <Navbar/>
            <div>
                Search: <br/>
                {s}{" "}{p}
            </div>
        </>
    )
}

import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Navbar from "../../../components/Navbar/Navbar";
import WebFooter from '../../../components/WebFooter/WebFooter';

export default function Home(props) {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(false);

  let componentMounted = true;

  useEffect(() => {
    const getProducts = async () => {
      setLoading(true);

      const res = await fetch(`https://localhost:44362/api/v1/products/lastest`);
      const res1 = await res.json();
      // console.log(res1.data);

      if (componentMounted) {
        //setProducts(await res.json().data);
        setProducts(res1.data);
        setLoading(false);
      }

      return () => {
        componentMounted = false;
      };
    };

    getProducts();
  }, []);

  const Loading = () => {
    return <div>Loading...</div>;
  };

  return (
    <>
      <Navbar />
      <div>
        <div className="card bg-dark text-white border-0">
          <img
            src="https://previews.123rf.com/images/arrow/arrow1508/arrow150800011/43834601-%E3%82%AA%E3%83%B3%E3%83%A9%E3%82%A4%E3%83%B3-%E3%82%B7%E3%83%A7%E3%83%83%E3%83%94%E3%83%B3%E3%82%B0-e-%E3%82%B3%E3%83%9E%E3%83%BC%E3%82%B9-%E3%83%95%E3%83%A9%E3%83%83%E3%83%88%E3%81%AA%E3%83%87%E3%82%B6%E3%82%A4%E3%83%B3-%E3%82%B3%E3%83%B3%E3%82%BB%E3%83%97%E3%83%88%E3%81%AE%E3%83%90%E3%83%8A%E3%83%BC%E3%81%AE%E8%83%8C%E6%99%AF.jpg"
            className="card-img"
            alt="..."
          />
          <div className="card-img-overlay"></div>
        </div>
        <div className="container mt-5 py-4">
          <div className="row">
            <div className="col-12 mb-2">
              <h2 className="display-6 fw-bolder text-center">
                Lastest Products
              </h2>
            </div>
          </div>
        </div>
        <div className="container-fluid">
          <div className="row">
            {loading == false ? (
              products.map(function (p) {
                return (
                  <div className="col col-sm-6 col-md-6 col-lg-3 mb-2" key={p.productID}>
                    <Link
                      to={`/products/${p.productID}`}
                      className="col-12 pb-6"
                      
                    >
                      <div className="card h-100" style={{ width: "18rem" }}>
                        <img
                          src={p.thumb}
                          className="card-img-top"
                          alt={p.description}
                        />
                        <div className="card-body">
                          <h4 className="card-title card-text mb-3">{p.name}</h4>
                          <div className="mb-2">
                            {p.description}
                          </div>
                          <div className="text-danger" style={{fontSize: 18, textAlign: 'right'}}>
                            {p.price} VNĐ
                          </div>
                          {/* <a href="/" className="btn btn-primary">
                            Go somewhere
                          </a> */}
                        </div>
                      </div>
                    </Link>
                  </div>
                );
              })
            ) : (
              <Loading></Loading>
            )}
          </div>
        </div>
      </div>
      <WebFooter/>
    </>
  );
}

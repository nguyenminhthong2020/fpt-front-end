/*eslint-disable*/
import React, { useEffect } from "react";
import Navbar from "../../../components/Navbar/Navbar";

import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useLocation } from "react-router-dom";
import { removeCart } from "../../../redux/slices/cart/cartSlice";

import WebFooter from "../../../components/WebFooter/WebFooter";

export default function CheckoutSuccess(props) {
  const location = useLocation();
  const dispatch = useDispatch();
  const navigate = useNavigate();

  // console.log(localStorage.getItem('refreshToken'))
  const addressId = localStorage.getItem("fptEcommerce_customerAddress_id");
  const addressLocation = localStorage.getItem(
    "fptEcommerce_customerAddress_location"
  );
  const isLoggin = useSelector((state) => state.auth.isLoggin);
  const current = useSelector((state) => state.auth.current);


  const check =
    isLoggin == false ||
    addressId == "undefined" ||
    addressLocation == "undefined" ||
    location.state == null;

  useEffect(() => {
    if (check) {
      navigate("/");
    } else {
      dispatch(removeCart());
    }
  }, []);


  return (
    <>
      <Navbar />
      {!check && (
        <div className="container" style={{ marginTop: 50 }}>
          <div
            className="row"
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "center",
            }}
          >
            <div className="col-sm-12 col-md-10 col-lg-9">
              <form>
                <div className="login-form">
                  <h2
                    className="login-title text-center"
                    style={{ fontFamily: "Times New Roman" }}
                  >
                    ĐẶT HÀNG THÀNH CÔNG
                  </h2>
                  <div className="row mt-3">
                    <div className="col-md-12 pb-4 text-center">
                      <p style={{ color: "#bac34e", fontSize: 17 }}>
                        {current.user.fullName}
                      </p>
                      <p>{current.user.email}</p>
                      <p>{current.user.phone}</p>
                      <p>{addressLocation}</p>
                    </div>
                    {location.state.cart.map(function (p) {
                      return (
                        <div
                          key={p.product.productID}
                          className="d-flex flex-row justify-content-between"
                          style={{ marginBottom: "10px", fontSize: "15px" }}
                        >
                          <div
                            style={{
                              fontWeight: "bold",
                              marginRight: "20px",
                            }}
                          >
                            - {p.product.name}
                          </div>
                          <div className="amount me-5">
                            {p.product.price} x {p.quantity} (VNĐ)
                          </div>
                          <div className="amount text-danger">
                            {p.product.price * p.quantity} VNĐ
                          </div>
                        </div>
                      );
                    })}
                    <hr />
                    <p style={{ textAlign: "right" }}>
                      <span>
                        TỔNG:{"\u00A0"}
                        {"\u00A0"}
                        {"\u00A0"}
                      </span>
                      <span style={{ fontSize: 16, color: "red" }}>
                        {location.state.totalMoney} VNĐ
                      </span>
                    </p>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      )}

      <WebFooter></WebFooter>
    </>
  );
}

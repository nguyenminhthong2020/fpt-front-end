/*eslint-disable*/
import React, { useEffect } from "react";
import Navbar from "../../../components/Navbar/Navbar";

import { useSelector, useDispatch } from "react-redux";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { updateInfo } from "../../../redux/slices/auth/authSlice";

import { useLocation } from "react-router-dom";
import { BASE_URL } from "../../../globals/constant";

import WebFooter from "../../../components/WebFooter/WebFooter";

export default function Checkout(props) {
  const location = useLocation();
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const addressId = localStorage.getItem("fptEcommerce_customerAddress_id");
  const addressLocation = localStorage.getItem(
    "fptEcommerce_customerAddress_location"
  );
  const isLoggin = useSelector((state) => state.auth.isLoggin);

  const current = useSelector((state) => state.auth.current);

  const cart = useSelector((state) => state.cart.cart);
  let sum = cart.reduce(
    (partialSum, a) => partialSum + a.product.price * a.quantity,
    0
  );

  const check =
    isLoggin == false ||
    addressId == "undefined" ||
    addressLocation == "undefined" ||
    location.state == null;

  useEffect(() => {
    if (check) {
      navigate("/");
    } else {
      const axiosInstance = axios.create({
        baseURL: BASE_URL,
        timeout: 10000,
        headers: {
          Authorization: "Bearer " + current.accessToken,
        },
      });
      axiosInstance
        .post("api/v1/customers/update-info", {
          fullname: location.state.fullname,
          phone: location.state.phone,
          email: location.state.email,
        })
        .then((res) => {
          if (res.data.success + "" == "true")
            dispatch(
              updateInfo({
                message: 'ok',
                fullname: location.state.fullname,
                phone: location.state.phone,
                email: location.state.email,
              })
            );
        })
        .catch((err) => {
          if ((err.response.status = 401)) {
            localStorage.removeItem("fptEcommerce_customerAddress_id");
            localStorage.removeItem("fptEcommerce_customerAddress_location");
            localStorage.removeItem("persist:root");
            navigate("/login/home");
          } else {
            notify(err.response.data.message);
          }
        });
    }
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    const cart1 = cart.map(function (item) {
      var x = {
        productId: item.id,
        quantity: item.quantity,
        totalMoney: item.quantity * item.product.price,
      };
      return x;
    });
    var data = {
      customerId: current.user.customerId,
      addressId: addressId,
      totalMoney: sum,
      cart: cart1,
    };

    const axiosInstance1 = axios.create({
      baseURL: BASE_URL,
      timeout: 5000,
      headers: {
        Authorization: "Bearer " + current.accessToken,
      },
    });

    axiosInstance1
      .post(`api/v1/orders/create`, data)
      .then((res) => {
        console.log(res.data);
        navigate("/checkout-success", {
          state: {
            totalMoney: sum,
            cart: cart,
          }
        })
      })
      .catch((err) => {
        console.log(err);
        navigate("/checkout-fail")
      });
    //console.log(data);
  };

  return (
    <>
      <Navbar />
      {!check && (
        <div className="container" style={{ marginTop: 50 }}>
          <div
            className="row"
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "center",
            }}
          >
            <div className="col-sm-12 col-md-10 col-lg-9">
              <form>
                <div className="login-form">
                  <h2
                    className="login-title text-center"
                    style={{ fontFamily: "Times New Roman" }}
                  >
                    XÁC NHẬN ĐẶT HÀNG
                  </h2>
                  <div className="row mt-3">
                    <div className="col-md-12 pb-4 text-center">
                      <p style={{ color: "#bac34e", fontSize: 17 }}>
                        {current.user.fullName}
                      </p>
                      <p>{current.user.email}</p>
                      <p>{current.user.phone}</p>
                      <p>{addressLocation}</p>
                    </div>
                    {cart.map(function (p) {
                      return (
                        <div
                          key={p.product.productID}
                          className="d-flex flex-row justify-content-between"
                          style={{ marginBottom: "10px", fontSize: "15px" }}
                        >
                          <div
                            style={{
                              fontWeight: "bold",
                              marginRight: "20px",
                            }}
                          >
                            - {p.product.name}
                          </div>
                          <div className="amount me-5">
                            {p.product.price} x {p.quantity} (VNĐ)
                          </div>
                          <div className="amount text-danger">
                            {p.product.price * p.quantity} VNĐ
                          </div>
                        </div>
                      );
                    })}
                    <hr />
                    <p style={{ textAlign: "right" }}>
                      <span>
                        TỔNG:{"\u00A0"}
                        {"\u00A0"}
                        {"\u00A0"}
                      </span>
                      <span style={{ fontSize: 16, color: "red" }}>
                        {sum} VNĐ
                      </span>
                    </p>
                    <div className="col-12 mt-2 text-center">
                      <button
                        type="submit"
                        className="btn btn-custom-size lg-size btn-secondary btn-primary-hover rounded-0"
                        onClick={handleSubmit}
                      >
                        Đặt hàng
                      </button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      )}

      <WebFooter></WebFooter>
    </>
  );
}

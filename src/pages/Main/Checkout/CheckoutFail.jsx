/*eslint-disable*/
import React, { useEffect } from "react";
import Navbar from "../../../components/Navbar/Navbar";

import { useSelector} from "react-redux";
import { useNavigate } from "react-router-dom";

import WebFooter from "../../../components/WebFooter/WebFooter";

export default function CheckoutFail(props) {
  const navigate = useNavigate();

  const isLoggin = useSelector((state) => state.auth.isLoggin);
  const current = useSelector((state) => state.auth.current);


  const check = isLoggin == false 

  useEffect(() => {
    if (check) {
      navigate("/");
    } 
  }, []);


  return (
    <>
      <Navbar />
      {!check && (
        <div className="container" style={{ marginTop: 50 }}>
          <div
            className="row"
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "center",
            }}
          >
            <div className="col-sm-12 col-md-10 col-lg-9">
              <form>
                <div className="login-form">
                  <h2
                    className="login-title text-center"
                    style={{ fontFamily: "Times New Roman" }}
                  >
                    ĐẶT HÀNG THẤT BẠI
                  </h2>
                  <div className="row mt-4">
                    <div className="col-md-12 pb-4 text-center">
                      <p style={{ fontSize: 18, color: 'red'}}>
                        Xin lỗi {current.user.fullName}, bạn vừa đặt hàng thất bại
                      </p>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      )}

      <WebFooter></WebFooter>
    </>
  );
}

/*eslint-disable*/
import React, {useState, useEffect} from "react";
import Navbar from "../../../components/Navbar/Navbar";

import { useSelector, useDispatch } from "react-redux";
//import { useForm, Controller } from "react-hook-form";
import { useNavigate } from "react-router-dom";
//import { updateInfoAsync } from "../../../redux/slices/auth/authSlice";

// import {
//   Link,
// } from "react-router-dom";

import WebFooter from "../../../components/WebFooter/WebFooter";

export default function CustomerInfomation(props) {
  const isLoggin = useSelector((state) => state.auth.isLoggin);
  const current = useSelector((state) => state.auth.current);
  //alert(JSON.stringify(current))
  //const dispatch = useDispatch();

  const navigate = useNavigate();

  
  const [listAddress, setListAddress] = useState([]);
  const [address, setAddress] = useState({
    addressId: 0,
    location: "",
  });

  let componentMounted = true;

  useEffect(()=>{
    if(isLoggin == false)
    {
       navigate("/login/home");
    }else{
      const getProducts = async () => {
  
        const res = await fetch(
          `https://localhost:44362/api/v1/addresses`
        );
        const res1 = await res.json();
  
        //console.log(res1);
        if (componentMounted) {
          setListAddress(res1.data);
        }
  
        return () => {
          componentMounted = false;
        };
      };
  
      getProducts();
    }
  }, [])
  
  useEffect(()=>{
    if(listAddress.length > 0)
    setAddress({
      location: listAddress[0].location,
      addressId: listAddress[0].addressId
    });
  }, [listAddress])

  
  const [fullname, setFullname] = useState(isLoggin == false ? "": current.user.fullName);
  const [phone, setPhone] = useState(isLoggin == false ? "": current.user.phone);
  const [email, setEmail] = useState(isLoggin == false ? "": current.user.email);

  const handleChangeAdress = (e) => {
    const m = e.target.value;
    setAddress({
      location: listAddress[m - 1].location,
      addressId: m
    })
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    // console.log(address)
    localStorage.setItem('fptEcommerce_customerAddress_id', address.addressId + "");
    localStorage.setItem('fptEcommerce_customerAddress_location', address.location + "");

    // dispatch(updateInfoAsync({ 
    //   accessToken: current.accessToken, 
    //   fullname: fullname, 
    //   phone: phone, 
    //   email: email, 
    //   address: address}));
    
      navigate('/checkout', {
        state: {
          fullname: fullname, 
      phone: phone, 
      email: email, 
      address: address,
        }
      });

    //console.log({fullname, phone, email, address})
  };

  return (
    <>
      <Navbar />
      <div className="container" style={{ marginTop: 50 }}>
        <div
          className="row"
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
          }}
        >
          <div className="col-sm-12 col-md-8 col-lg-8">
            <form>
              <div className="login-form">
                <h2
                  className="login-title"
                  style={{ fontFamily: "Times New Roman" }}
                >
                  CUNG CẤP THÔNG TIN KHÁCH HÀNG
                </h2>
                <div className="row ">
                <div className="col-md-12 pb-4 mt-2">
                    <label className="text-danger">Username:{" "}{isLoggin == true ? current.user?.username : ""}</label>
                  </div>

                  <div className="col-md-12 pb-3">
                    <label>Fullname* </label>
                    <input type="fullname" id="fullname" name="fullname" value={fullname} onChange={(e)=>setFullname(e.target.value)}/>
                  </div>
                  <div className="col-md-12 pb-3">
                    <label>Phone* </label>
                    <input type="phone" id="phone" name="phone" value={phone} onChange={(e)=>setPhone(e.target.value)}/>
                  </div>

                  <div className="col-md-12 pb-3">
                    <label>Email </label>
                    <input type="email" id="email" name="email" value={email} onChange={(e)=>setEmail(e.target.value)}/>
                  </div>

                  <div className="col-md-12 pb-5 mb-2">
                    <label>Address </label>
                    {/* https://reactjs.org/docs/forms.html */}
                    <select value={address.addressId} onChange={handleChangeAdress}>
                    {
                      listAddress.map(function(item){
                        return <option value={item.addressId} key={item.addressId}>{item.location}</option>
                      })
                    }
                    </select>
                  </div>

                  <div className="col-12">
                    <button
                      type="submit"
                      className="btn btn-custom-size lg-size btn-secondary btn-primary-hover rounded-0"
                      onClick={handleSubmit}
                    >
                      Xác nhận
                    </button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <WebFooter></WebFooter>
    </>
  );
}

import React from "react";
import Home from "../pages/Main/Home/Home";
import Login from "../pages/Auth/Login";
import Products from "../pages/Main/Product/Products";
import ProductDetail from "../pages/Main/Product/ProductDetail";
import About from "../pages/Main/About/About";
import Contact from "../pages/Main/Contact/Contact";
import Cart from "../pages/Main/Cart/Cart";
import CustomerInfomation from "../pages/Main/Checkout/CustomerInfomation";
//import Test from "../pages/Main/Test/Test";


import { Routes, Route } from "react-router-dom";
import Checkout from "../pages/Main/Checkout/Checkout";
import CheckoutSuccess from "../pages/Main/Checkout/CheckoutSuccess";
import CheckoutFail from "../pages/Main/Checkout/CheckoutFail";

export default function MainRoute(props) {
  return (
    <>
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route exact path="/products/pages" element={<Products/>} />
        <Route exact path="/products/:id" element={<ProductDetail/>} />
        <Route exact path="/login/:returnUrl" element={<Login />} />
        <Route exact path="/about" element={<About />} />
        <Route exact path="/contact" element={<Contact />} />
        <Route exact path="/cart" element={<Cart />} />
        <Route exact path="/customer-information" element={<CustomerInfomation />} />
        <Route exact path="/checkout" element={<Checkout/>} />
        <Route exact path="/checkout-success" element={<CheckoutSuccess/>} />
        <Route exact path="/checkout-fail" element={<CheckoutFail/>} />
      </Routes>
    </>
  );
}
